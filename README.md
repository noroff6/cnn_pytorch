# CNN with PyTorch

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy](https://www.experis.se/sv/it-tjanster/experis-academy). 
<br />

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [References](#references)
- [License](#license)


## Background


## Loss 
Tells the model how far off its estimation was from the actual value. In the image below the loss for the 2 different models are compared.
![](loss.png)

## Accuracy
Comparing accuracy between the 2 models.
![](model_acc.png)

## Install

- [Jupyter notebook](https://www.anaconda.com/)
- [Python 3.9](https://www.python.org/)
- [PyTorch](https://pytorch.org/)
- [Torchvision](https://pytorch.org/vision/stable/index.html)
- [Matplotlib](https://matplotlib.org/)
- [OpenCv](https://opencv.org/)
- [sklearn](https://scikit-learn.org/stable/index.html)


## Usage
The code is developed with Jupyter notebooks. Make sure to have all the dependencies installed before clonning the repo. The handwritten small alphabet can be replaced by your own generated handwritten dataset.



## Contributing
For any questions reach out to Helena Barmer.


## References
- [Handwritten Digit Recognition Using PyTorch — Intro To Neural Networks](https://towardsdatascience.com/handwritten-digit-mnist-pytorch-977b5338e627)



## License
[MIT](https://en.wikipedia.org/wiki/MIT_License)